package com.plugins.menthions.group;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

//macro
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.core.task.MultiQueueTaskManager;
import com.atlassian.mywork.service.LocalNotificationService;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;

public class Macro implements com.atlassian.confluence.macro.Macro {

	private final XhtmlContent xhtmlUtils;
	private Notifications notify;
	private MacroLog log = new MacroLog();

	public Macro(XhtmlContent xhtmlUtils, final LocalNotificationService notificationService,
			final MultiQueueTaskManager taskManager) {
		this.xhtmlUtils = xhtmlUtils;
		notify = new Notifications(notificationService, taskManager);
	}

	@Override
	public String execute(Map<String, String> parameters, String bodyContent, ConversionContext conversionContext)
			throws MacroExecutionException {
		log.debug("Start Execute Macro Tag-Groups");

		OutputBuilder builder = new OutputBuilder();
		List<String> groups = new ArrayList<String>();

		for (String group : parameters.keySet()) {
			// HACK - come from the parameters without any make sens reason...
			if (group.contains("RAW"))
				continue;

			groups.add(group);
		}

		log.debug("start send notifications");
		String pageUrl = conversionContext.getEntity().getUrlPath();
		String pageTitle = conversionContext.getEntity().getTitle();
		String pageId = conversionContext.getEntity().getIdAsString();
		notify.notifyAll(groups, pageUrl, pageTitle, pageId);

		Map context = MacroUtils.defaultVelocityContext();
		context.put("groupNames", groups);

		return builder.GetFromTemplate(context, "onpage_template");
	}

	@Override
	public BodyType getBodyType() {
		return BodyType.NONE;
	}

	@Override
	public OutputType getOutputType() {
		return OutputType.BLOCK;
	}

}