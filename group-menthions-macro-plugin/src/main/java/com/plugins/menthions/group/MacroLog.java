package com.plugins.menthions.group;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class MacroLog {
	private String LOG_TITLE = "Tag-Group";

	private static final Log log = LogFactory.getLog(Macro.class);

	public MacroLog() {

	}

	public void debug(String message) {
		log.debug(LOG_TITLE + " " + message);
	}

	public void error(String message) {
		log.error(LOG_TITLE + " " + message);
	}

	public void info(String message) {
		log.info(LOG_TITLE + " " + message);
	}
}
