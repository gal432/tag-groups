package com.plugins.menthions.group;

import com.atlassian.mywork.service.LocalNotificationService;
import com.plugins.menthions.group.notifications.ConfluenceNotification;
import com.plugins.menthions.group.notifications.MailNotification;
import com.plugins.menthions.group.notifications.INotification;
import com.atlassian.core.task.MultiQueueTaskManager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;

public class Notifications {
	private Collection<INotification> notifications;
	private UserSearch userSearch;

	public Notifications(LocalNotificationService notificationService, MultiQueueTaskManager taskManager) {
		this.notifications = new ArrayList<INotification>();
		this.notifications.add(new MailNotification(taskManager));
		this.notifications.add(new ConfluenceNotification(notificationService));

		this.userSearch = new UserSearch(notificationService);
	}

	public void notifyAll(Collection<String> groupToNotify, String pageUrl, String pageTitle, String pageId) {

		Hashtable<String, User> notifyUsers = this.userSearch.getUsersToNotify(groupToNotify, pageId);
		notifyToUsers(notifyUsers, pageUrl, pageTitle, pageId);
	}

	private void notifyToUsers(Hashtable<String, User> notifyUsers, String pageUrl, String pageTitle, String pageId) {

		String corrUser;
		Enumeration<String> users = notifyUsers.keys();

		while (users.hasMoreElements()) {
			corrUser = users.nextElement();

			for (INotification noti : this.notifications) {
				noti.sendNotification(notifyUsers.get(corrUser), pageTitle, pageUrl, pageId);
			}
		}
	}
}
