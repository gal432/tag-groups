package com.plugins.menthions.group;

import java.util.Map;
import com.atlassian.confluence.util.velocity.VelocityUtils;

public class OutputBuilder {

	public String GetFromTemplate(Map context, String templateName) {
		try {
			String result = VelocityUtils.getRenderedTemplate("templates/" + templateName + ".vm", context);
			return result;
		} catch (Exception e) {

		}
		return null;
	}

}
