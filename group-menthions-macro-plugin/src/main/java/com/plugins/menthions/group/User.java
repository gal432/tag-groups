package com.plugins.menthions.group;

import java.util.ArrayList;
import java.util.List;

import com.atlassian.confluence.user.ConfluenceUser;

public class User {
	private ConfluenceUser confUser;

	private List<String> groups;

	public User(ConfluenceUser usr) {
		this.setConfUser(usr);
		this.setGroups(new ArrayList<String>());
	}

	public List<String> getGroups() {
		return groups;
	}

	public void setGroups(List<String> groups) {
		this.groups = groups;
	}

	public ConfluenceUser getConfUser() {
		return confUser;
	}

	public void setConfUser(ConfluenceUser confUser) {
		this.confUser = confUser;
	}

}
