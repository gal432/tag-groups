package com.plugins.menthions.group;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;

import org.codehaus.jackson.JsonNode;

import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.mywork.model.Notification;
import com.atlassian.mywork.service.LocalNotificationService;
import com.atlassian.spring.container.ContainerManager;
import com.atlassian.user.EntityException;
import com.atlassian.user.Group;
import com.atlassian.user.GroupManager;

public class UserSearch {
	private UserAccessor userAccessor = (UserAccessor) ContainerManager.getComponent("userAccessor");
	private GroupManager groupManager = (GroupManager) ContainerManager.getComponent("groupManager");

	private final String TAG_GROUPS_APP_ID = "TAG-GROUP-APPID";
	private LocalNotificationService notificationService;
	private MacroLog log = new MacroLog();

	public UserSearch(LocalNotificationService notificationService) {
		this.notificationService = notificationService;
	}

	public Hashtable<String, User> getUsersToNotify(Collection<String> groupToNotify, String pageId) {
		Hashtable<String, User> notifyUsers = new Hashtable<String, User>();

		for (String group : groupToNotify) {
			try {
				for (ConfluenceUser confluenceUser : getUsersToNotifyInGroup(group, pageId)) {
					User usr = searchOrCreateUser(notifyUsers, confluenceUser);
					usr.getGroups().add(group);
				}
			} catch (Exception x) {
				log.error("Failed to load group - " + group);
			}
		}
		return notifyUsers;
	}

	private User searchOrCreateUser(Hashtable<String, User> notifyUsers, ConfluenceUser confluenceUser) {
		User usr;
		if (notifyUsers.containsKey(confluenceUser.getName())) {
			usr = notifyUsers.get(confluenceUser.getName());
		} else {
			usr = new User(confluenceUser);
			notifyUsers.put(confluenceUser.getName(), usr);
		}
		return usr;
	}

	private Iterable<ConfluenceUser> getUsersToNotifyInGroup(String groupName, String pageId)
			throws EntityException {
		Group confUsersGroup;
		List<ConfluenceUser> users = new ArrayList<ConfluenceUser>();
		try {
			confUsersGroup = groupManager.getGroup(groupName);

			if (confUsersGroup == null)
				throw new EntityException();

			for (ConfluenceUser user : userAccessor.getMembers(confUsersGroup)) {
				if (needToNotifyToUser(user, pageId)) {
					users.add(user);
				}
			}
			return users;

		} catch (EntityException e) {
			throw e;
		}
	}

	private boolean needToNotifyToUser(ConfluenceUser user, String pageId) {

		for (Notification noti : notificationService.findAll(user.getName())) {
			String app = noti.getApplication();
			JsonNode node = noti.getMetadata().findValue("key");
			
			if (app != null && app.equals(TAG_GROUPS_APP_ID) && node != null && node.asText().contains(pageId)) {
				return false;
			}
		}
		return true;
	}

}
