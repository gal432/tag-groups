package com.plugins.menthions.group.mail;

import com.atlassian.mail.queue.MailQueueItem;

public interface MailService {

	/**
	 * This will send an email based on the details stored in the
	 * ConfluenceMailQueueItem
	 *
	 * @param mailQueueItem
	 *            the item to send
	 */
	void sendEmail(MailQueueItem mailQueueItem);
}