package com.plugins.menthions.group.notifications;

import java.util.Collection;
import java.util.Map;

import org.codehaus.jackson.node.JsonNodeFactory;
import org.codehaus.jackson.node.ObjectNode;

import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.mywork.model.NotificationBuilder;
import com.atlassian.mywork.service.LocalNotificationService;
import com.plugins.menthions.group.OutputBuilder;
import com.plugins.menthions.group.User;

public class ConfluenceNotification implements INotification {

	private LocalNotificationService notificationService;

	public ConfluenceNotification(LocalNotificationService notificationService) {
		this.notificationService = notificationService;
	}

	@Override
	public void sendNotification(User user, String title, String pageUrl, String pageId) {

		String desc = buildDescription(user, title, user.getGroups());
		String firstGroup = "";
		for (String group : user.getGroups()) {
			firstGroup = group;
			break;
		}
		title = "Group " + firstGroup + " mentioned in " + title;
		
		//write id
		JsonNodeFactory facory = JsonNodeFactory.instance;
		ObjectNode id = new ObjectNode(facory);
		id.put("key", pageId);
		
		com.atlassian.mywork.model.Notification notification = new NotificationBuilder().title(title)
				.itemTitle(title).metadata(id).description(desc).url(pageUrl).application(TAG_GROUPS_APP_ID)
				.createNotification();
		notificationService.createOrUpdate(user.getConfUser().getName(), notification);

	}

	private String buildDescription(User user, String title, Collection<String> groups) {

		Map context = MacroUtils.defaultVelocityContext();
		context.put("pageTitle", title);
		context.put("groupNames", groups);

		OutputBuilder builder = new OutputBuilder();
		return builder.GetFromTemplate(context, "notification_template");
	}
}
