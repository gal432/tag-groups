package com.plugins.menthions.group.notifications;
import com.plugins.menthions.group.User;

public interface INotification {
	String TAG_GROUPS_APP_ID = "TAG-GROUP-APPID";

	void sendNotification(User user, String title, String pageUrl, String pageId);
}
