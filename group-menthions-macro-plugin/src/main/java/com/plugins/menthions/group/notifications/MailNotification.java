package com.plugins.menthions.group.notifications;

import static com.atlassian.confluence.mail.template.ConfluenceMailQueueItem.MIME_TYPE_HTML;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.atlassian.confluence.mail.template.ConfluenceMailQueueItem;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.core.task.MultiQueueTaskManager;
import com.atlassian.mail.queue.MailQueueItem;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.spring.container.ContainerManager;
import com.plugins.menthions.group.OutputBuilder;
import com.plugins.menthions.group.User;
import com.plugins.menthions.group.mail.MailService;
import com.plugins.menthions.group.mail.MailServiceImpl;

public class MailNotification implements INotification {
	private MailService mailService;
	private SettingsManager settingsManager = (SettingsManager) ContainerManager.getComponent("settingsManager");

	public MailNotification(MultiQueueTaskManager taskManager) {
		this.mailService = new MailServiceImpl(taskManager);
	}

	@Override
	public void sendNotification(User user, String title, String pageUrl, String pageId) {
		String desc = buildDescription(user.getConfUser().getName(), title, user.getGroups(), pageUrl);

		pageUrl = settingsManager.getGlobalSettings().getBaseUrl() + pageUrl;

		MailQueueItem mailQueueItem = new ConfluenceMailQueueItem(user.getConfUser().getEmail(), title, desc, MIME_TYPE_HTML);
		mailService.sendEmail(mailQueueItem);
	}

	private String buildDescription(String user, String title, Collection<String> groups, String pageUrl) {
		Map context = MacroUtils.defaultVelocityContext();
		context.put("pageTitle", title);
		context.put("pageUrl", pageUrl);
		context.put("groupNames", groups);

		OutputBuilder builder = new OutputBuilder();
		return builder.GetFromTemplate(context, "mail_template");
	}

}
