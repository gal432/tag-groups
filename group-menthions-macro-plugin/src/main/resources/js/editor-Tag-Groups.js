function filterGroups()
{
    var groupName = document.getElementById('group-filter-text').value;
	var resultList = document.getElementById('founded-groups');
    var groups = AJS.params.groupNamesCsv.split(",");
    var notifyGroups = document.getElementById("groups-to-notify").children;
	
	cleanSelect("founded-groups");
	if(groupName.length < 3)
	{
		alert("Search at least 3 chars");
		return;
	}
	
	
    for(i=0; i < groups.length ;i++)
    {
        corr = groups[i];
        if(corr.length > 0 && corr.indexOf(groupName) != -1 && exsistInGroup(notifyGroups, corr)==false)
        {
            var newOption = document.createElement("option");
            newOption.text = corr;
            resultList.appendChild(newOption);
        }
    }
}

function cleanSelect(elementName)
{
	var resultList = document.getElementById(elementName);
	all_childs = resultList.children;
	
	while(resultList.children.length!=0)
	{
		resultList.removeChild(all_childs[0]);
	}
}
            
function exsistInGroup(group, groupName)
{
    length = group.length;

    for(j=0; j<length;j=j+1)
    {
        text = group[j].text;

        if(text.localeCompare(groupName)==0)
        {
            return true;
        }
    }
    return false;
}
            
function insertToNotify()
{
    //create new group to notify
    var e = document.getElementById("founded-groups");
    var newOption = document.createElement("option");
    newOption.text = e.options[e.selectedIndex].text;

    //add to result list
    var resultList = document.getElementById("groups-to-notify");
    if(exsistInGroup(resultList.children, newOption.text)==false)
	{
		resultList.appendChild(newOption);
	}
	var toDelete = e.options[e.selectedIndex];
	e.removeChild(toDelete);
}
            
function removeFromNotify()
{
    var e = document.getElementById("groups-to-notify");
    var toDelete = e.options[e.selectedIndex];
    e.removeChild(toDelete);
}

function insertToDialogTagedGroups(groups)
{
	cleanSelect("groups-to-notify");

	length = groups.length;
	for(i=0; i<length; i++)
	{
		var e = document.getElementById("groups-to-notify");
		var newOption = document.createElement("option");
		newOption.text = groups[i];
		e.appendChild(newOption);
	}
}

AJS.bind("init.rte", function() { 
    var macroName = 'Tag-Groups';

    // 1. create dialog to add macro
    var dialog = new AJS.Dialog(865, 530);

	dialog.addHeader("Add Groups to Tag");
	
	var body = "\
	<table class='aui' id='basic'>\
		<thead>\
			<tr>\
				<th>Search Group\
				<br>\
					<label>1 - Search for group to notify</label>\
				</th>\
				<th>Results\
				<br>\
					<label>2 - Add the groups to notify</label>\
				</th>\
			</tr>\
		</thead>\
		<tbody>\
			<tr>\
				<th>\
					<input type='text' class='text macro-param-input' id='group-filter-text' style='width:300px'>\
					<input id='start-filter' type='button' value='Filter' class='button-panel-button button-panel-submit-button' onclick='filterGroups();' />\
				</th>\
				<th>\
					<select id='founded-groups' class='founded-groups' style='width:300px'></select>\
					<input id='add-to-notify' class='button-panel-button button-panel-submit-button' type = 'button' value='Add' onclick='insertToNotify();' />\
				</th>\
			</tr>\
		</tbody>\
		</table>\
		\
		<table class='aui' id='basic'>\
		<thead>\
			<tr>\
				<th><center>Groups to notify</center></th>\
			</tr>\
		</thead>\
		<tbody>\
		<tr>\
				<th>\
					<center>\
						<select id='groups-to-notify' class='groups-to-notify' style='width:300px'></select>\
						<input id='remove-from-notify' type = 'button' value='Remove' onclick='removeFromNotify();' />\
					</center>\
				</th>\
			</tr>\
		</tbody>\
		</table>"
		
		
	panel = dialog.addPanel("Search Groups", body,  "panel-body");
	
	
    // hide dialog
    dialog.addCancel("Cancel", function() {
        dialog.hide();
    });

    // 2. add macro to editor
    dialog.addSubmit("Save", function() {
		var currentParams = {};
		
		groupsToNotify = document.getElementById("groups-to-notify").children;
		
		for(i=0; i<groupsToNotify.length; i++)
		{
			currentParams[groupsToNotify[i].text] = groupsToNotify[i].text;
		}
        // 3. get current selection in editor
        var selection = AJS.Rte.getEditor().selection.getNode();
        var macro = {
            name: macroName,
			body: "",
			params: currentParams,
        };

        // 4. convert macro and insert in DOM
		tinymce.confluence.macrobrowser.macroBrowserComplete(macro);
        dialog.hide();
    });
	
    // 5. bind event to open macro browser
    AJS.MacroBrowser.setMacroJsOverride(macroName, {opener: function(macro) {
        // open custom dialog
        dialog.show();
		insertToDialogTagedGroups(Object.keys(macro.params));
    }});
});